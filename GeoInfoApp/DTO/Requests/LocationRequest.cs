﻿using System.ComponentModel.DataAnnotations;

namespace GeoInfoApp.DTO.Requests
{
    public class LocationRequest
    {
        public int LocationId { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }

        [Required]
        public string Address { get; set; }

        public string Reference { get; set; }
    }
}