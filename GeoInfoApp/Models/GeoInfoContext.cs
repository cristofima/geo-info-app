﻿using Microsoft.EntityFrameworkCore;

namespace GeoInfoApp.Models
{
    public class GeoInfoContext : DbContext
    {
        public GeoInfoContext()
        {
        }

        public virtual DbSet<Location> Locations { get; set; }

        public GeoInfoContext(DbContextOptions<GeoInfoContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasKey(e => e.LocationId);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}