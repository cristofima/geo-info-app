﻿using System.ComponentModel.DataAnnotations;

namespace GeoInfoApp.Models
{
    public class Location
    {
        [Key]
        public int LocationId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Address { get; set; }

        public string Reference { get; set; }
    }
}