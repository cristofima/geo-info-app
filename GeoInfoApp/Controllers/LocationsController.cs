﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeoInfoApp.Models;
using GeoInfoApp.DTO.Requests;

namespace GeoInfoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly GeoInfoContext _context;

        public LocationsController(GeoInfoContext context)
        {
            _context = context;
        }

        // GET: api/Locations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Location>>> GetLocations()
        {
            return await _context.Locations.ToListAsync();
        }

        // GET: api/Locations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Location>> GetLocation(int id)
        {
            var location = await _context.Locations.FindAsync(id);

            if (location == null)
            {
                return NotFound();
            }

            return location;
        }

        // POST: api/Locations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Location>> PostLocation(LocationRequest request)
        {
            Location location;

            if (request.LocationId > 0)
            {
                location = _context.Locations.Find(request.LocationId);

                if (location != null)
                {
                    location.Address = request.Address;
                    location.Latitude = request.Latitude;
                    location.Longitude = request.Longitude;
                    location.Reference = request.Reference;

                    _context.Update(location);
                }
            }
            else
            {
                location = new Location()
                {
                    Address = request.Address,
                    Latitude = request.Latitude,
                    Longitude = request.Longitude,
                    Reference = request.Reference
                };

                _context.Locations.Add(location);
            }

            await _context.SaveChangesAsync();

            return Ok(location);
        }

        // DELETE: api/Locations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLocation(int id)
        {
            var location = await _context.Locations.FindAsync(id);
            if (location == null)
            {
                return NotFound();
            }

            _context.Locations.Remove(location);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}