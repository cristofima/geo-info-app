FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["GeoInfoApp/GeoInfoApp.csproj", "GeoInfoApp/"]
RUN dotnet restore "GeoInfoApp/GeoInfoApp.csproj"
COPY . .
WORKDIR "/src/GeoInfoApp"
RUN dotnet build "GeoInfoApp.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "GeoInfoApp.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
CMD ASPNETCORE_URLS=http://*:$PORT dotnet GeoInfoApp.dll
